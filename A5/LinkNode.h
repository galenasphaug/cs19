/**
   Galen Asphaug (gasphaug)
   <galenasphaug@gmail.com>
   CS19 Looking Out for Number One
   LinkNode.h working/tested 4-25-2018
**/

#ifndef LINK_NODE__H
#define LINK_NODE__H

class LinkNode {
  //friend class DigitAnalyzer;
 public:
  LinkNode();
  LinkNode(int);
  LinkNode(int, LinkNode*);
  ~LinkNode();

  //int getData() const;
  //void setData(int d);
  //private:
  int data;
  LinkNode * next;
  //LinkNode * prev;
};


#endif /*LINK_NODE__H*/
