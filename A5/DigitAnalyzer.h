/**
   Galen Asphaug (gasphaug)
   <galenasphaug@gmail.com>
   CS19 Looking Out for Number One
   DigitAnalyzer.h working/tested 5-3-2018
**/

#ifndef DIGIT_ANALYZER__H
#define DIGIT_ANALYZER__H

#include "LinkNode.h"

class DigitAnalyzer {
 public:
  DigitAnalyzer();
  ~DigitAnalyzer();
  
  // Linked lists are LIFO
  
  // Push a node to the head of the list.
  // If the head of the list is a nullptr,
  // set the head of the list to equal the new node.
  //void push_head(LinkNode * n);
  void push_head(int n);
  
  // Return the head and remove it from the list
  // Head gets set to the node that is attatched to it
  // If head->next is a nullptr, set head as a nullptr.
  //LinkNode * pop_head();
  int pop_head();
  
  // Return the size of the list
  unsigned getSize() const;

  // Delete all nodes with data=0, and sets the
  // previous node to point to the node after the zero(es)
  unsigned countAndRemoveZeroEntries();

  // Find the first digit of each node and increment the array index
  // based on what the first digit is.
  //@return an array of size 10
  unsigned * getStartingDigits() const;
  
  // Print the list to stdout
  void print() const;
  
 private:
  unsigned size;
  LinkNode * head;
};

#endif /*DIGIT_ANALYZER__H*/
