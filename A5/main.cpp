/**
   Galen Asphaug (gasphaug)
   <galenasphaug@gmail.com>
   CS19 Looking Out for Number One
   main.cpp working/untested 4-29-2018
**/

#include <iostream>
#include <fstream>
#include <sstream>
#include "DigitAnalyzer.h"

using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::string;

const string OPEN_COMMENT = "(*";
const string CLOSE_COMMENT = "*)";

int main(int argc, char ** argv) {
  DigitAnalyzer l;
  string filename = "";

  int val;
    
  // Assume argv[1] is a filename
  if (argc == 2) {
    filename = argv[1];
    cout << "Filename: " << filename << endl;
    std::ifstream fin;
    fin.open(filename);
    if (!fin) {
      cerr << "Can't find " << filename << endl;
      exit(0);
    }

    string line;

    // Push all the values in
    while (getline(fin, line)) {

      std::istringstream ss;
      std::size_t open_index; // Where the comment is located (if any)
      std::size_t close_index; // These will be string::npos

      open_index = line.find(OPEN_COMMENT);
      close_index = line.find(CLOSE_COMMENT);

      //cout << open_index << "->" << close_index << '(' << line.length() << ')' << endl;

      //string::npos is returned if no match
      if (open_index == string::npos && close_index == string::npos && line.length() > 0) {
	ss.str(line);
      } else { // If there are comments in the line
	if (open_index == 0) { // If line starts with a comment
	  //if (close_index != string::npos) { // If there is a closing comment
	  ss.str(line.substr(close_index + 2)); // Add the data after the comment
	  //} else { // If comment was not closed
	  //  cerr << "Error: invalid formatting:\n" << line << endl;
	  //}
	} else if (close_index + 2 == line.length()) { // If the closing comment is at the end
	  //if (open_index != string::npos) { // If there is an opening comment
	  ss.str(line.substr(0, open_index)); // Add the data before the comment
	  //} else { // If comment was not opened
	  //  cerr << "Error: invalid formatting:\n" << line << endl;
	  //}
	}
      }

      if (!(open_index == 0 && close_index == line.length())) {
	//cout << '\"' << ss.str() << '\"' << endl;
	ss >> val;
	//cout << "Pushing " << val << endl;
	l.push_head(val);
	val = 0;
      }
    } // End file while loop
    fin.close();
  } else {
    //Get input from user if no file specified
    while (cin >> val) {
      l.push_head(val);
    }
  }
  // Print the list
  l.print();
  cout << l.countAndRemoveZeroEntries() << " zeroes removed\n";
  l.print();
}
