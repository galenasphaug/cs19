/**
   Galen Asphaug (gasphaug)
   <galenasphaug@gmail.com>
   CS19 Looking Out for Number One
   DigitAnalyzer.cpp working/tested 5-3-2018
**/

#include <iostream>
#include "DigitAnalyzer.h"
#include "LinkNode.h"


DigitAnalyzer::DigitAnalyzer() {
  head = nullptr;
  //tail = nullptr;
  size = 0;
}

DigitAnalyzer::~DigitAnalyzer() {
  LinkNode * temp;
  while (head) {
    temp = head;
    head = head->next; // Advance head before we delete it
    delete temp;
  }
}

// void DigitAnalyzer::push_head(LinkNode* n) {
//   if (head) {
//     n->next = head;
//     head = n;
//     size++;
//   } else head = n;
//   return head;
// }

void DigitAnalyzer::push_head(int n) {
  LinkNode * node = new LinkNode(n, head);
  head = node;
  size++;
}

// LinkNode* DigitAnalyzer::pop_head() {
//   LinkNode * result = head;
//   if (head) {
//     size--;
//     head = head->next;
//   }
//   return result;
// }

int DigitAnalyzer::pop_head() {
  if (!head) {
    std::cerr << "error: pop_head(): No values in list to pop.\nsize = " << size << std::endl;
    return 0;
  }
  int d = head->data;
  LinkNode * temp = head;
  head = head->next;
  size--;
  delete temp; temp = nullptr;
  return d;
}

unsigned DigitAnalyzer::getSize() const {
  return size;
}


//TODO: Fix removing 3 zeroes in a row!
unsigned DigitAnalyzer::countAndRemoveZeroEntries() {
  unsigned count = 0;
  LinkNode * temp = new LinkNode(0, head); //Temp points to head
  LinkNode * old = temp;
  while (temp) {
    // If next node is a zero, remove it from the list
    if (temp->next && temp->next->data == 0) {
      LinkNode * skippedNode = temp->next;
      //First next is not null, next next COULD be null
      temp->next = temp->next->next;
      // If we delete the head, make the next node the new head
      if (skippedNode == head) head = skippedNode->next;
      // Delete the middle 0 node
      delete skippedNode; skippedNode = 0;
      count++;
    } else {
      // If it's not a 0, advance and repeat if not null
      temp = temp->next; // temp->next might be null
    }
  } //temp = nullptr;
  size -= count;
  delete old; old = nullptr;
  return count;
}

unsigned * DigitAnalyzer::getStartingDigits() const {
  unsigned * digits = new unsigned[9];
  // Initialize to zero values
  for (int i = 0; i < 10; i++) {
    digits[i] = 0;
  }
  LinkNode * temp = head;
  while (temp) {
    int d = abs(temp->data); // Count negatives too!
    // Divide d by 10 until we get the first digit
    while (d >= 10) {
      d /= 10;
    }
    digits[d]++;
    temp = temp->next;
  }
  return digits;
}

void DigitAnalyzer::print() const {
  std::cout << size << " nodes\n";
  if (size == 0) return;
  unsigned* digits = getStartingDigits();
  
  for (unsigned i = 0; i <= 9; i++) {
    std::cout << i << "s:\t" << digits[i] << '\t'
	      << (double)digits[i] * 100 / (double)size << '%' << std::endl;
  }
  delete digits;
}

// Print each value:
// LinkNode * temp = head;
// while (temp) {
//   std::cout << temp->data << std::endl;
//   temp = temp->next;
// }
