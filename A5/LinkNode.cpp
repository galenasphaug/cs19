/**
   Galen Asphaug (gasphaug)
   <galenasphaug@gmail.com>
   CS19 Looking Out for Number One
   LinkNode.cpp working/tested 4-25-2018
**/

#include "LinkNode.h"

LinkNode::LinkNode() {
  data = 0;
  next = nullptr;
  //prev = nullptr;
}

LinkNode::LinkNode(int d) {
  data = d;
  next = nullptr;
  //prev = nullptr;
}

LinkNode::LinkNode(int d, LinkNode * n) {
  data = d;
  next = n;
}

LinkNode::~LinkNode() {
  // Dont delete attatched nodes, let ~LinkedList() handle that
  //if (next) delete next;
  //if (prev) delete prev;
  next = nullptr;
  //prev = nullptr;
}

// int LinkNode::getData() const {
//   return data;
// }

// void LinkNode::setData(int d) {
//   data = d;
// }
