/**
   Galen Asphaug (gasphaug@pengo) <galenasphaug@gmail.com>
   board.h - Final #1 - 3D Tic-Tac-Toe
*/

#ifndef BOARD__H
#define BOARD__H

#include <string>

const char X_CHAR = 'X';
const char O_CHAR = 'O';

// For addressing a specific point on the board
struct location {
  unsigned int x, y, z;
};

// The 3D board
class Board {
public:
  Board();
  char grid[3][3][3];

  unsigned int getUserValue(std::string);
  location getUserLocation();
  
  void setChar(char, location);
  char getChar(location);
  
  bool isEmpty(location);
  bool isX(location);
  bool isO(location);
  
  void clearBoard();
  void print();
  
  void play();
  bool gameOver();
private:
  
};


#endif /* BOARD__H */
