/**
   Galen Asphaug (gasphaug@pengo) <galenasphaug@gmail.com>
   board.cpp - Final #1 - 3D Tic-Tac-Toe
*/

#include "board.h"
#include <iostream>

using std::cin;
using std::cout;
using std::cerr;
using std::endl;

// Set all values to a space
Board::Board() {
  Board::clearBoard();
}

// Get a single coordinate from stdin with a prompt, only allowing values 0-2
unsigned int Board::getUserValue(std::string s) {
  unsigned int result = 3;
  do {
    cout << s;
    cin >> result;
  } while (result >= 3);
  return result;
}

// Get a location from the user
location Board::getUserLocation() {
  location l;
  l.z = getUserValue("Which layer?  (0-2): ");
  l.y = getUserValue("Which row?    (0-2): ");
  l.x = getUserValue("Which column? (0-2): ");
  return l;
}

// Set the character at the location
void Board::setChar(char c, location l) {
  grid[l.z][l.y][l.x] = c;
}

// Get the character at the location
char Board::getChar(location l) {
  return grid[l.z][l.y][l.x];
}

// Set all values to a space
void Board::clearBoard() {
  for (unsigned int z = 0; z <= 2; z++) {
    for (unsigned int y = 0; y <= 2; y++) {
      for (unsigned int x = 0; x <= 2; x++) {
	grid[z][y][x] = ' ';
      }
    }
  }
}

// Print the entire grid to stdout
void Board::print() {
  for (int z = 2; z >= 0; z--) { // Layers count down
    cout << "Layer " << z << endl;
    for (unsigned int y = 0; y <= 2; y++) { // cols
      cout << "-------\n";
      for (unsigned int x = 0; x <= 2; x++) { // rows
	cout << '|' << grid[z][y][x];
      }
      cout << '|' << endl;
    }
    cout << endl;
  }
}

// Start the game
void Board::play() {
  location lX;
  location lO;

  while (!gameOver()) {
    cout << X_CHAR << "'s turn!\n";
    lX = getUserLocation();
    print();
    cout << O_CHAR << "'s turn!\n";
    lO = getUserLocation();
    print();
  }
  //checkForWin();
}

bool Board::gameOver() {
  char winner = ' ';
  // Check each layer for wins
  for (unsigned z = 0; z <= 2; z++) {
    // Check each column for wins
    for (unsigned y = 0; y <= 2; z++) {
      if (grid[z][y][0] != ' ' &&
	  grid[z][y][0] == grid[z][y][1] &&
	  grid[z][y][1] == grid[z][y][2]) winner = grid[z][y][0];
    }

    // Check each row for wins
    for (unsigned x = 0; x <= 2; x++) {
      if (grid[z][0][x] != ' ' &&
	  grid[z][0][x] == grid[z][1][x] &&
	  grid[z][1][x] == grid[z][2][x]) winner = grid[z][0][x];
    }
    // Check diagonals upper left to lower right
    if (grid[z][0][0] != ' ' &&
	  grid[z][0][0] == grid[z][1][1] &&
	  grid[z][1][1] == grid[z][2][2]) winner = grid[z][0][0];

    // Check diagonals lower left to upper right
    if (grid[z][2][0] != ' ' &&
	  grid[z][2][0] == grid[z][1][1] &&
	  grid[z][1][1] == grid[z][0][2]) winner = grid[z][2][0];
  }

  //TODO: Check for wins across layers ///////////////////
  
  if (winner != ' ') {
    cout << "Game Over! " << winner << " wins!\n";
    return true;
  } else return false;
}
