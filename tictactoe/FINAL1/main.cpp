/**
   Galen Asphaug (gasphaug@pengo) <galenasphaug@gmail.com>
   main.cpp - Final #1 - 3D Tic-Tac-Toe
*/

#include "board.h"
#include <iostream>

using std::cin;
using std::cout;
using std::cerr;
using std::endl;

int main() {
  Board b;
  //b.play();
  b.setChar(X_CHAR, b.getUserLocation());
  
  b.print();
}
