// CS20j Final Exam - 3D Tic Tac Toe Game
// (C) Steve J. Hodges, Computer Science, Cabrillo College
// as per academic policy:
//     DO NOT DUPLICATE, DO NOT DISTRIBUTE, DO NOT TRANSCRIBE
//     for your individual use only

// directions: 
// You are to write the code for readLogo() and winner()
// (those two functions are at the bottom of the file)
//             ONLY MODIFY THE CONTENTS OF THE TWO
//             ASSIGNED FUNCTIONS AND ADD YOUR NAME ETC
//             WHERE DIRECTED OR ADD IMPORT STATEMENTS
//             DO NOT MODIFY OTHER PARTS OF THE CODE 



// YOU NAME AND PENGO LOGIN HERE PLEASE


#include <iostream>
#include <fstream>
#include <cstdlib>


class TicTacToeSolution{
private:
  static const std::string SIGIL[3]; // board graphics
  static const int CCODE[3]; // color codes
  static const int WINS[49][3]; // winning combos
  static const int LOGOH=5;
  static const int LOGOW=60;
  int logo[LOGOH][LOGOW]; // array that stores the game logo
  static const int SIZE = 3;
  static const int SIZE3D = SIZE*SIZE*SIZE;
  // board    :27 positions as follows
  // top         middle      bottom 
  // 00 01 02    09 10 11    18 19 20 
  // 03 04 05    12 13 14    21 22 23 
  // 06 07 08    15 16 17    24 25 26 
  //
  // value: 0 is empty
  //        1 is player 1 (X)
  //        2 is player 2 (O)
  int gameBoard[SIZE3D];

  
  // convert board, row, col value in range 0-2 to 
  // a board array index
  // invalid values generate an index of -1
  int calculateIndex(int board, int row, int col){
    if (board <0 || board >=SIZE ||
	row <0   || row >=SIZE   ||
	col <0   || col >=SIZE     ){
      return -1; // invalid index
    }
    return board * (SIZE * SIZE ) + row * SIZE + col;
  }


  // Changes text color of System.out using ANSI standard CSI escape codes
  void color(int c){
    switch(c){
    case 1: // black bold on green
      std::cout <<('\u001B');std::cout <<("[0;30;1;42m");break;  
    case 2: // white bold on green
      std::cout <<('\u001B');std::cout <<("[0;37;1;42m");break;  
    case 3: // yellow on green
      std::cout <<('\u001B');std::cout <<("[0;33;42m");break;  
    case 4: // bold green on black
      std::cout <<('\u001B');std::cout <<("[0;32;1;40m");break;  
    case 5: // bold green on white
      std::cout <<('\u001B');std::cout <<("[0;32;1;47m");break;  
    default:
    case 0:   // black on white (default)
      std::cout <<('\u001B');std::cout <<("[0;30m");break;  
    }
  }


  // game ends when either player1 or player 2 wins
  bool gameNotOver(){
    return (winner() == 0);
  }
  
  
  void drawLogo(){
    std::cout << "Final Exam: 3D Tic Tac Toe - YOUR NAME HERE";
    color(0);
    std::cout << std::endl;
    for(int i=0;i<LOGOH;i++){
      for(int j=0;j<LOGOW;j++){
	color(logo[i][j]+3);
	std::cout << " ";
      }
      color(0);
      std::cout <<std::endl;
    }
    color(0);
  }
  
  
  void drawBoard(){
    int index=0;
    color(0);
    std::cout << std::endl;
    std::cout <<"   [0] top        [1] middle      [2] bottom "<<std::endl;
    std::cout <<"   0  1  2         0  1  2         0  1  2 "<<std::endl;
    for (int row = 0; row < SIZE; row++){
      color(0);
      std::cout << row << ":";
      for (int board = 0; board < SIZE; board++){
	for (int col = 0; col < SIZE; col++){
	  index = calculateIndex(board, row, col);
	  color( CCODE[gameBoard[index]] );
	  std::cout << SIGIL[gameBoard[index]];
	  color(0);
	} // col
	std::cout << "       "; // space between boards
      } // board
      std::cout << std::endl; // next row
    } // row
    color(0);
    std::cout << std::endl;
  }


  // player's turn
  // input move until a legal move is chosen
  // and then update the gameBoard
  void playerTurn(int player){
    int board = -1; // init to invalid choices
    int col = -1;
    int row = -1;
    int index = -1;
    
    // repeat until valid move is made
    while( index == -1 || gameBoard[index] != 0){
      color(player);
      std::cout << "Player "<< player <<" "<< SIGIL[player] << "  your move (b r c)? ";
      color(0);
      std::cin >> board >> row >> col;
      index = calculateIndex(board, row, col);
    }
    // add players move to the game board
    gameBoard[index] = player;
  }
  
  
  int winner();
  void readLogo();

public:
  TicTacToeSolution(){
    for(int i=0; i<SIZE3D; i++)
      gameBoard[i] = 0;
  }
  

  void playGame(){
    int player = 1; // indicates current player
    
    readLogo();
    drawLogo();
    for(int i=0;i<SIZE3D;i++){
      gameBoard[i] = 0; // empty
    }
    
    // main game loop
    while(gameNotOver()){
      drawBoard();
      playerTurn(player);
      player = 3 - player; // other players turn
    }
    // show final board
    drawBoard();
    int win = winner();
    color(win);
    std::cout << "Player " << win << " " << SIGIL[win] << " wins!";
    color(0);
    std::cout << std::endl;
  }


};

const std::string TicTacToeSolution::SIGIL[3] = {" . ", "(X)", "(O)"};
const int TicTacToeSolution::CCODE[3] = {3, 1, 2};
const int TicTacToeSolution::WINS[49][3]  = { {0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {9, 10, 11}, {12, 13, 14},
				     {15, 16, 17}, {18, 19, 20}, {21, 22, 23}, {24, 25, 26}, {0, 10, 20},
				     {2, 10, 18}, {3, 13, 23}, {5, 13, 21}, {6, 16, 26}, {8, 16, 24},

				     {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {9, 12, 15}, {10, 13, 16},
				     {11, 14, 17}, {18, 21, 24}, {19, 22, 25}, {20, 23, 26}, {0, 12, 24},
				     {6, 12, 18}, {1, 13, 25}, {7, 13, 19}, {2, 14, 26}, {8, 14, 20},

				     {0, 4, 8}, {2, 4, 6}, {9, 13, 17}, {11, 13, 15}, {18, 22, 26},
				     {20, 22, 24}, {0, 13, 26}, {8, 13, 18}, {2, 13, 24}, {6, 13, 20},

				     {0, 9, 18}, {1, 10, 19}, {2, 11, 20}, {3, 12, 21}, {4, 13, 22},
				     {5, 14, 23}, {6, 15, 24}, {7, 16, 25}, {8, 17, 26} };



// returns value to indicate winner
// 0 = no one has won yet
// 1 or 2 for player 1 or 2 win
// check all 49 ways to win
int TicTacToeSolution::winner(){
  // placeholder code:
  return 0; //no one has won yet
}   


// this function should open the text file "logo-ttt.txt"
// and store values '0' = 0 ,  '1' = 1 ,  '2' = 2
// into the integer array named logo
void TicTacToeSolution::readLogo(){
  // placeholder code:
  for(int i=0;i<LOGOH;i++){
    for(int j=0;j<LOGOW;j++){
      logo[i][j] = std::rand() % 3;
    }
  }
}




int main(){
  TicTacToeSolution ttt;
  ttt.playGame();
  return 0;
}

