/**
   Galen Asphaug (gasphaug@pengo) <galenasphaug@gmail.com>
   Dictionary.cpp - AS4 - Bingo
   Computes anagrams of words using maps and vectors
*/

#include <iostream>
#include <fstream>
#include "Dictionary.h"

Dictionary::Dictionary() {
  dictionary = new map<string, vector<string> >;
}

Dictionary::Dictionary(string filename) {
  dictionary = new map<string, vector<string> >;
  if (!open(filename)) {
    cerr << "Error: Could not open " << filename
	 << "... trying to read "  << DEFAULT_FILENAME << endl;
    if (!open(DEFAULT_FILENAME)) {
      cerr << "Cannot open default file " << DEFAULT_FILENAME << "\nExiting...\n";
      exit(1);
    }
  }
}

Dictionary::~Dictionary() {
  delete dictionary; dictionary = nullptr;
} 

vector<string>* Dictionary::find(string s) {
  return &(*dictionary)[sortLetters(s)];
}

void Dictionary::add(string s) {
  (*dictionary)[sortLetters(s)].push_back(s);
}

bool Dictionary::open(string filename) {
  std::ifstream fin;
  fin.open(filename);
  
  if (!fin) return false;
  
  string line;
  while (getline(fin, line)) {
    unsigned i = 0;
    while (i < line.length()) {
      // Strip invalid characters (TODO: maybe a stringstream instead?)
      if (islower(line[i])) {
	i++;
      } else {
	line.erase(i, 1);
      }
    }
    add(line);
  }
  fin.close();
  return true;
}

string Dictionary::sortLetters(string s) {
  if (s.length() < 2) return s;
  for (unsigned i = 0; i < s.length() - 1; i++) {
    unsigned min = i; //index of next smallest value
    for (unsigned j = i + 1; j < s.length(); j++) {
      //Find next smallest character after s[i]
      if (s[j] < s[min]) min = j;
    }
    if (min != i) {
      char temp = s[i];
      s[i] = s[min];
      s[min] = temp;
    }
    //std::cout << s << std::endl;
  }
  return s;
}
