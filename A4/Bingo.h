/**
   Galen Asphaug (gasphaug@pengo) <galenasphaug@gmail.com>
   Bingo.h - AS4 - Bingo
   Plays a guessing game using a Dictionary object
*/

#ifndef BINGO__H
#define BINGO__H

#include <iostream>
#include "Dictionary.h"

using std::cin;
using std::cout;
using std::endl;

class Bingo {
 public:
  // Initialize the Dictionary
  Bingo();
  
  // Initialize and read file
  Bingo(string filename);

  // Delete Dictionary data
  ~Bingo();

  // Print a list of matches to stdin, asks for new guess if not 7 characters
  bool play(string guess);
  
  // Prompt and get a 7 letter guess from stdin
  const static string getGuess();
  
  // Returns the parameter following the flag, or
  // an empty string if there is no flag or string following the flag
  static string parse(int argc, char ** argv, string flag);
  
 private:
  Dictionary* d;
};

#endif /*BINGO__H*/
