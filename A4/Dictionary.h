/**
   Galen Asphaug (gasphaug@pengo) <galenasphaug@gmail.com>
   Dictionary.h - AS4 - Bingo
   Computes anagrams of words using maps and vectors
*/

#ifndef DICTIONARY__H
#define DICTIONARY__H

#include <vector>
#include <map>
#include <iostream>

using std::string;
using std::vector;
using std::map;
using std::cout;
using std::cerr;
using std::endl;

const string DEFAULT_FILENAME = "unixdict.txt";

class Dictionary {
 public:
  // Initialize map
  Dictionary();
  
  // Initialize and read data
  Dictionary(string filename);

  // Delete map
  ~Dictionary();

  // Find word pairs
  vector<string>* find(string s);
  
  // Insert string into map (key = sorted word, value = word)
  void add(string s);

  // Open, read, and add all lines in a file
  // excluding all but lowercase letters
  // returns false if it cant open the file, otherwise true
  bool open(string filename);

  //Simple selection sort
  static string sortLetters(string s);
  
 private:
  // Stores vectors of words using the sorted words as keys
  // EX: key:abcd values:{badc, cabd, bdca, abcd}
  map<string, vector<string> >* dictionary;
};

#endif /*DICTIONARY__H*/
