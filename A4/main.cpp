/**
   Galen Asphaug (gasphaug@pengo) <galenasphaug@gmail.com>
   main.cpp - AS4 - Bingo
*/

#include <iostream>
#include <cstdlib> //exit()
#include <cctype>
#include <fstream>
#include <vector>

#include "Bingo.h"
#include "Dictionary.h"

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::ifstream;
using std::ofstream;
using std::fstream;

int main(int argc, char ** argv) {
  string f = Bingo::parse(argc, argv, "-f");
  string g = Bingo::parse(argc, argv, "-g");

  Bingo* b;
  
  if (f.length() >= 1) {
    b = new Bingo(f);
  } else {
    b = new Bingo(DEFAULT_FILENAME);
  }
 
  
  if (g.length() < 1) {
    //cout << "g null";
    g = Bingo::getGuess();
  }

  if ( !(b->play(g)) ) {
    cout << "No matches for " << g << endl;
  }

  delete b; b = nullptr;

}
