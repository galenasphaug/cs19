/**
   Galen Asphaug (gasphaug@pengo) <galenasphaug@gmail.com>
   Bingo.cpp - AS4 - Bingo
   Plays a guessing game using a Dictionary object
*/

#include <iostream>
#include "Bingo.h"

Bingo::Bingo() {
  d = new Dictionary();
}

Bingo::Bingo(string filename) {
  d = new Dictionary(filename);
}

Bingo::~Bingo() {
  delete d; d = nullptr;
}

bool Bingo::play(string guess) {
  // Make sure the Dictionary is filled first
  if (!d) {
    cerr << "Error: No data in Dictionary. Trying to read " << DEFAULT_FILENAME << "...\n";
    if (!d->open(DEFAULT_FILENAME)) {
      cerr << "Error: Cannot open default file " << DEFAULT_FILENAME << "\nExiting...\n";
      exit(1);
    }
  }
  
  if (guess.length() != 7) {
    cout << "Guess must be 7 letters, try again\n";
    guess = getGuess(); //either guess is 7 chars, or program exits
  }
  
  // point to a vector of matching words and print them
  vector<string>* words = d->find(guess);      //add a charac
  if (!words || words->size() < 1) return play(guess + guess[0]);
  for (size_t i = 0; i < words->size(); i++) {
    if ((*words)[i].length() == guess.length()) {
      cout << (*words)[i] << endl;
    }
  }
  return true;
}

const string Bingo::getGuess() {
  string guess = "";
  cout << "Enter a guess: ";
  while (cin >> guess && guess.length() != 7) {
    cout << "Invalid input. Guess must be 7 letters.\nEnter a guess: ";
  }
  return guess;
}


string Bingo::parse(int argc, char ** argv, string flag) {
  //string param = "";
  // Follow argv backwards to find last flag and stay in bounds
  // Skip last string because it should be the parameter to the flag
  for (int i = argc - 2; i >= 1; i--) {
    if (argv[i] == flag) {
      return argv[i + 1];
    }
  }
  //std::cout << "No " << flag << " flag\n";
  return "";
}
