// CS19:Final Exam
// Linked List with Templates exercise
// Filename: linkedlistf.cpp
// as per Academic Integrity policy: for your individual use only
// DO NOT DUPLICATE, DO NOT TRANSCRIBE, DO NOT DISTRIBUTE
// Instructions: modify ONLY the bodies of the two indicated functions,
//               and add your name/email to the comment and the cout in main()
//               do not change any other part of the code
//               see handout for further details
//
// edit the following line:
// Galen Asphaug @gasphaug

#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <sstream>
using namespace std;




// data structure that's just wierd
class RidiculistItem{
public:
  friend ostream &operator<<(ostream &os, RidiculistItem &item);
  RidiculistItem(double left=0.0, double right=0.0){ _left = left; _right = right;}
  bool operator<(RidiculistItem &other);
  RidiculistItem operator+(RidiculistItem &augend);
  RidiculistItem operator+=(RidiculistItem &augend);
private:
  double _left;
  double _right;
};

ostream &operator<<(ostream &os, RidiculistItem &item){
  os << item._left << " + " << item._right << " = " <<item._left + item._right;
  return os;
}

bool RidiculistItem::operator<(RidiculistItem &other){
  return (_left+_right) < (other._left + other._right);
}

RidiculistItem RidiculistItem::operator+(RidiculistItem &addend){
  // sum = augend + addend
  RidiculistItem sum;
  RidiculistItem &augend = *this; 
  sum._left = augend._left + addend._left;
  sum._right = augend._right + addend._right;
  return sum;
}

RidiculistItem RidiculistItem::operator+=(RidiculistItem &addend){
  RidiculistItem &sum = *this; 
  sum._left += addend._left;
  sum._right += addend._right;
  return sum;
}

// ---------------------------------------------------------------------

template <class T>
class List;

template <class T>
class LinkNode{
  // only allow friend access to the list destructor
  // and print function
  friend List<T>::~List();
  friend void List<T>::print();
public:
  LinkNode(T *d=0, LinkNode *n=0 ){next=n; data=d;}
  ~LinkNode(){ if (data){ delete data; data = 0;} }
  void setData( T *d){ data = d; }
  void setNext(LinkNode *n){ next = n; }
  T *getData(){ return data; }
  LinkNode *getNext(){ return next; }
private:
  T *data;
  LinkNode *next;
};

template <class T>
class List{
public:
  List(){ first = 0; size = 0; }
  ~List();
  void print();
  void insertCopy( T *);
  pair<T, T>getMaxAndSum();
private:
  LinkNode<T> *first;
  int size;
};

// this function is a linknode friend
template <class T>
List<T>::~List(){
  LinkNode<T> *link = first;
  while (first){
    link = first;
    first = first->next;
    delete link;
  }
}

// this function is a linknode friend
template <class T>
void List<T>::print(){
  LinkNode<T> *temp = first;
  while (temp){
    cout << *(temp->data) << endl;
    temp = temp -> next;
  }
}

// ******* This is one of the functions to code
template <class T>
void List<T>::insertCopy(T *item){
  if (item) { // assert item not null
    // make a LinkNode and copy item to data
    T * newData = new T(*item); // copy data, LinkNode will handle deleting
    LinkNode<T> * node = new LinkNode<T>(newData, 0); // use newData in new LinkNode
    
    if (first) { // if list has first element
      node->setNext(first); // make its next item point to the 'first'
      first = node;
      size++;
    } else {
      first = node; // it is the first item
      size = 1;
    }
  }
}

// ******* This is one of the functions to code
template <class T>
pair<T,T> List<T>::getMaxAndSum(){
  // placeholder code: 
  // modify at your preference
  T sum;
  LinkNode<T> *temp = first;
  T max = 0; // = *(temp->getData());
  // if max = 0 by default, there is a problem when the max is negative
  // if max = temp->data by default, there is a chance that temp->data could be null
  while (temp){
    T * dataPointer = temp->getData(); // to avoid repeated calls to getData()
    //if (temp->getData) { // if there is data (cant do != 0 or != nullptr)
    sum += *dataPointer; // add it to the sum
    if (! (*dataPointer < max) ) // use ! after we evaluate a boolean (if not less than)
      max = *dataPointer;        // so we dont have to code RidiculistItem::operator>() (if greater than)
    //}
    temp = temp->getNext(); // move to next item
  }
  
  pair<T,T> result;
  result.first = max;
  result.second = sum;
  return result;
}



// ---------------------------------------------------------


/* main output should look (mostly) like this:

CS19 Linked List Exam
YOUR NAME AND PENGO LOGIN HERE PLEASE

*   For the List<int> with values:
-3
9
7
5
    the max is 9
    the sum is 18

*   For the List<RidiculistItem> with values: 
0.2 + 0.4 = 0.6
5.2 + 2.1 = 7.3
3.2 + 1.4 = 4.6
    the max is 5.2 + 2.1 = 7.3
    the sum is 8.6 + 3.9 = 12.5

*/

main(){
  cout << "CS19 Linked List Exam" << endl;
  cout << "Galen Asphaug @gasphaug" << endl;
  List<int> intList;
  int x = 5; int y = 7; int z = 9; int w = -3;
  intList.insertCopy(&x);
  intList.insertCopy(&y);
  intList.insertCopy(&z);
  intList.insertCopy(&w);
  cout << endl << "*   For the List<int> with values:" << endl;
  intList.print();
  pair<int, int> result = intList.getMaxAndSum();
  cout << "    the max is " << result.first << endl;
  cout << "    the sum is " << result.second << endl;
  
  RidiculistItem *reusedPointer=0;
  List<RidiculistItem> whoa;
  reusedPointer = new RidiculistItem(3.2, 1.4);
  whoa.insertCopy(reusedPointer);
  delete reusedPointer;
  reusedPointer = new RidiculistItem(5.2, 2.1);
  whoa.insertCopy(reusedPointer);
  delete reusedPointer;
  reusedPointer = new RidiculistItem(0.2, 0.4);
  whoa.insertCopy(reusedPointer);
  delete reusedPointer;
  cout << endl << "*   For the List<RidiculistItem> with values: " << endl;
  whoa.print();
  pair<RidiculistItem, RidiculistItem> whoaResult = whoa.getMaxAndSum();
  cout << "    the max is " << whoaResult.first << endl;
  cout << "    the sum is " << whoaResult.second << endl;
}

