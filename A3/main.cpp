/**
   Galen Asphaug (gasphaug)
   <galenasphaug@gmail.com>
   CS19 Integer Sets Program
   Start Code by Steve J. Hodges
   main.cpp working/tested 2018-3-15
**/

#include <iostream>
#include "IntSet.h"
using std::cout;
using std::endl;

int main(){

   IntSet is1,is2(2000),is3;
   IntSet is4(is2);
   cout << "CS 19 Integer Sets" << endl;
   cout << "Galen Asphaug <galenasphaug@gmail.com>" << endl;
   
   is1.insertElement(2);    is1.insertElement(4);
   is1.insertElement(2);    is1.insertElement(3);
   is1.insertElement(5);    is1.insertElement(7);
   is1.deleteElement(3);    is1.deleteElement(7);
   is1.deleteElement(9);

   is4.insertElement(1087); is3.insertElement(1086);
   is2.insertElement(2);

   for(int i=0; i < (is3.getUpperLimit()*0.0125); i++)
     is3.insertElement( std::rand()%is3.getUpperLimit() );
   cout << "is3 (\"random\"): ";
   cout << is3.toString();
   cout << endl;

   cout << "is1: ";
   cout << is1.toString(); 
   cout << endl;

   cout << "is2: ";
   cout << is2.toString(); 
   cout << endl;

   is3.unionOf(is1, is2);
   cout << "is3 (union is1 is2): ";
   cout << is3.toString(); 
   cout << endl;

   is3.intersectionOf(is1, is2);
   cout << "is3 (intersection is1 is2): ";
   cout << is3.toString(); 
   cout << endl;

   if (is3 == is3)
     cout << "is3 == is3"<<endl;
   else 
     cout << "is3 != is3"<<endl;
   if (is1 == is2)
     cout << "is1 == is2"<<endl;
   else 
     cout << "is1 != is2"<<endl;
   if (is3 == is2)
     cout << "is3 == is2"<<endl;
   else 
     cout << "is3 != is2"<<endl;

   is1.deleteElement(4);
  
   cout << "is1: ";
   cout << is1.toString(); 
   cout << endl;

   if (is3 == is1)
     cout << "is3 == is1"<<endl;
   else 
     cout << "is3 != is1"<<endl;

   cout << "is4: ";
   cout << is4.toString(); 
   cout << endl;

   is4.unionOf(is2, is2);
   cout << "is4: ";
   cout << is4.toString(); 
   cout << endl;

   if (is1.hasElement(5) ){
     cout <<"is1 contains 5"  << endl; 
   } else {
     cout <<"is1 does not contain 5"  << endl;
   }

   if (is1.hasElement(7) ){
     cout <<"is1 contains 7"  << endl;
   } else {
     cout <<"is1 does not contain 7"  << endl; 
   }

   return 0;
}
