/**
   Galen Asphaug (gasphaug)
   <galenasphaug@gmail.com>
   CS19 Integer Sets Program
   intset.cpp working/tested 2018-3-15
**/

#include <iostream>
#include <sstream>
#include "IntSet.h"

// Constructors
IntSet::IntSet(int limit) {
  if (limit < 0) limit = 0;
  upperLimit = limit;
  data = new bool[upperLimit + 1];
}

// Copy constructor
IntSet::IntSet(const IntSet & set) {
  unsigned limit = set.getUpperLimit();
  upperLimit = limit;
  bool* temp = new bool[limit + 1];
  for (unsigned i = 0; i <= limit; i++) {
    temp[i] = set.hasElement(i);
  }
  data = temp;
}

// Destructor
IntSet::~IntSet() {
  //Delete set data and set pointer to null
  delete [] data; data = nullptr;
}

// Adds an element to the set
void IntSet::insertElement(int e) {
  if (e >= 0) {
    if (e > upperLimit) {
      //std::cout << "Resizing to " << e << std::endl;
      resize(e);
    }
    //std::cout << "Adding " << e << std::endl;
    data[e] = true;
  } else {
    std::cerr << "Warning: Discarded insert of "
	      << e << ", min value is 0.\n";
  }
}

// Removes an element from the set
void IntSet::deleteElement(int e) {
  if (e <= upperLimit && e >= 0) {
    //std::cout << "Deleting " << e << std::endl;
    data[e] = false;
  }
}

// Returns true if the set contains element e
bool IntSet::hasElement(int e) const {
  if (e <= upperLimit && e >= 0) {
    return data[e];
  }
  return false;
}

// Display the object as a string of included elements
std::string IntSet::toString() const {
  /* find the last value first, so we know 
     when to not print a trailing comma ("{1, 2, }") */
  
  /* It would be better to make this a member variable
     and set it for each new biggest element that gets added.
     That would skip exessive for loops on each toString(). */
  int last = -1;
  for (int i = upperLimit; i >= 0 && last < 0; i--) {
    if (data[i])
      last = i; //dont break for loop, instead check if last was set >=0
  }
  
  std::stringstream ss;
  ss << '{';
  for (int i = 0; i <= upperLimit; i++) {
    if (data[i]) {
      ss << i;
      if (i < last)
	ss << ", ";
    }
  }
  ss << "}";
  return ss.str();
}

// Stores the union of sets a and b in data
void IntSet::unionOf(const IntSet& a, const IntSet& b) {
  int max = IntSet::maxOf(a.getUpperLimit(), b.getUpperLimit());
  //int min = IntSet::minOf(a.getUpperLimit(), b.getUpperLimit());
  bool* tempUnion = new bool[max + 1];
  for (int i = 0; i <= a.getUpperLimit(); i++) {
    tempUnion[i] = a.hasElement(i);
  }

  for (int i = 0; i < b.getUpperLimit(); i++) {
    tempUnion[i] = tempUnion[i] || b.hasElement(i);
  }
  delete [] data;
  data = tempUnion;
}

// Stores the intersection of sets a and b in data
void IntSet::intersectionOf(const IntSet& a, const IntSet& b) {
  int max = IntSet::maxOf(a.getUpperLimit(), b.getUpperLimit());
  //int min = IntSet::minOf(a.getUpperLimit(), b.getUpperLimit());
  bool* tempUnion = new bool[max + 1];
  for (int i = 0; i <= a.getUpperLimit(); i++) {
    tempUnion[i] = a.hasElement(i);
  }

  for (int i = 0; i < b.getUpperLimit(); i++) {
    tempUnion[i] = tempUnion[i] && b.hasElement(i);
  }
  delete [] data;
  data = tempUnion;
}

// Overloaded equality operator
bool IntSet::operator==(const IntSet& set) const {
  if (&set == this) {
    return true;
  }
  int max = IntSet::maxOf(upperLimit, set.getUpperLimit());
  for (int i = 0; i < max; i++) {
    if (this->hasElement(i) != set.hasElement(i))
      return false;
  }
  return true;
}
