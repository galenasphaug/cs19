/**
   Galen Asphaug (gasphaug)
   <galenasphaug@gmail.com>
   CS19 Integer Sets Program
   Start Code by Steve J. Hodges
   intset.h working/tested 2018-3-14
**/

#ifndef INTSET__H
#define INTSET__H

class IntSet{
public:
  IntSet(int = DEFAULTUPPERLIMIT);
  IntSet(const IntSet &);
  ~IntSet();
  void insertElement(int);
  void deleteElement(int);
  bool hasElement(int) const;
  void unionOf(const IntSet &, const IntSet &);
  void intersectionOf(const IntSet &, const IntSet &);
  bool operator==(const IntSet &) const;
  std::string toString() const;
  int getUpperLimit()const{ return upperLimit; }
private:
  // create a new array with a larger or smaller capacity
  // and update upperLimit variable
  void resize(int n){ 
    if(n == upperLimit) return; // same size, nothing to do
    bool *temp = new bool[n+1]; // allow up to n
    for(int i=0; i <= n; i++)
      temp[i] = hasElement(i); // preserve old contents
    delete [] data; // remove old array
    data = temp;  // swap in new array
    upperLimit = n; // update limit
  }
  static int maxOf(int a, int b){ return (a > b) ? a : b; }
  static int minOf(int a, int b){ return (a < b) ? a : b; }
  int upperLimit;
  //int last;
  static const int DEFAULTUPPERLIMIT = 1000;
  bool *data; // must hold zero through upperLimit inclusive
};
#endif //INTSET__H
