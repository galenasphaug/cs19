#include <iostream>
#include <iomanip> //setprecision()
#include <vector>
#include <cctype> //ispunct()


//TODO: Skip spaces and punctuation

using std::cout;
using std::cin;
using std::string;
using std::endl;
using std::vector;

const int ALPHABETSIZE = 26;

string formatInput(const string& input);

void getPairs(const string& text, vector< vector<unsigned> >& count, int& total);

bool hasValues(const vector<unsigned> &count);

void print(const vector<vector<unsigned> > &count, const int& total);

int main() {
  //counter for each character and its possible neighbor
  vector< vector<unsigned> > count(ALPHABETSIZE, vector<unsigned> (ALPHABETSIZE, 0));
  int total;
  string text;
  string line;

  // fill text line by line
  while (getline(cin, line)) {
    text += line + '\n';
  }

  getPairs(formatInput(text), count, total);

  print(count, total);
  return 0;
}

void getPairs(const string& text, vector< vector<unsigned> >& count, int& total) {
  total = 0;
  for (int i = 0; i < (text.length() - 2)/*skip last index*/ && text.length() > 1; i++) {
    if (!ispunct(text[i]) && !ispunct(text[i+1])) { //both are alphanumeric
      if (text[i+1] == '\n') {//check for new lines and break up pairs
        i++; //skip to next
      } else if (text[i] == '\n') {
        //if skipped to the char after the newline and it's ALSO a newline
        //dont do anything and let the for loop skip to the next value
      } else {
        char a = text[i] - 'a';
        char b = text[i+1] - 'a';
        count[a][b]++; //ascii to array. a=0,b=1...z=25
        total++;
      }
    }
  }
}

//alphabet to lower case, all other characters are ignored.
string formatInput(const string& input) {
  string output;
  for (unsigned i = 0; i < input.length(); i++) {
    if (!ispunct(input[i]) && !isdigit(input[i]) && input[i] != ' ') {
      output += tolower(input[i]);
    }
  }
  return output;
}

void print(const vector<vector<unsigned> > &count, const int& total) {
  cout << "Letter Pair Frequency Table\n";
  //is    2    20.0%";

  for (int i = 0; i < ALPHABETSIZE; i++) {
    if (hasValues(count[i])) {
      for (int j = 0; j < ALPHABETSIZE; j++) {
        unsigned valueInVector = count[i][j];
        if (valueInVector > 0) {
          cout << std::setprecision(2) << (char)('a'+i) << (char)('a'+j) << '\t'
               << valueInVector << '\t' << ((double)valueInVector/total)*10.0 << '%' << endl;
        }
      }
    }
  }
  cout << total << " letter pairs in total\n";
}

bool hasValues(const vector<unsigned> &count) {
  for (int i = 0; i < count.size(); i++) {
    if (count[i] > 0)
      return true;
  }
  return false;
}

