/**
   loan.cpp - Calculates the number of payments needed to pay a loan
   working/tested
 */


#include <iostream>
#include <cmath> //log

using std::cin;
using std::cout;

/**
   Calculate interest per payment
   @param percent the annual interest rate
   @param perYear the number of payments each year
*/
double calculateInterest(double percent, double perYear);

/**
   Calculate the number of payments
   @param principal the amount borrowed
   @param interest interest rate per payment
   @param payment monthly payment
   
 */
double calculateNumpay(double principal, double interest, double payment);

int main() {
  double principal = 0.0;
  double annualInterest = 0.0;
  int paymentsPerYear = 0;
  double payment = 0.0;

  // User input
  cout << "Galen Asphaug <gasphaug@pengo, galenasphaug@gmail.com>\n";
  cout << "Assignment 1 - loan.cpp - Spring 2018\n";
  cout << "\nWhat is the amount in dollars to be borrowed? ";
  cin >> principal;
  cout << "What is the annual interest rate (%)? ";
  cin >> annualInterest;
  cout << "How many payments will be made each year? ";
  cin >> paymentsPerYear;
  cout << "What is the amount of each payment? ";
  cin >> payment;

  double interest = calculateInterest(annualInterest, paymentsPerYear);
  
  //cout << interest << std::endl;
  
  cout << "\nNumber of payments needed: " << calculateNumpay(principal, interest, payment)
       << std::endl;
  return 0;
}


double calculateInterest(double percent, double perYear) {
  return percent / (perYear * 100);
}

double calculateNumpay(double principal, double interest, double payment) {
  return - log(1 - interest * (principal / payment)) / log(1 + interest);
}
